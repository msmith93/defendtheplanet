package com.BeefGames.PlutoWasAPlanetOnce.View.Handlers;

import java.util.Date;

import com.swarmconnect.Swarm;
import com.swarmconnect.SwarmAchievement;
import com.swarmconnect.SwarmAchievement.AchievementUnlockedCB;
import com.swarmconnect.SwarmActiveUser.GotCloudDataCB;

public class AchievementHandler {
	
	private GotCloudDataCB callback;
	private int kills;
	private boolean classic,time,nightmare,android;

	public AchievementHandler() {
		android = true;
		callback = new GotCloudDataCB() {
		    public void gotData(String data) {

		        // Did our request fail (network offline, and uncached)?
		        if (data == null) {

		            // Handle failure case.
		            return;
		        }

		        // Has this key never been set?  Default it to a value...
		        if (data.length() == 0) {
		        
		            // In this case, we're storing levelProgress, default them to level 1.
		            data = "1";
		        }

		        // Parse the level data for later use
		        kills = Integer.parseInt(data);
		    }
		}; 
		
	}
	
	public void addKill(){
		
		String killString = Integer.toString(kills);
		
		Swarm.user.getCloudData("kills", callback);
		
		if(kills == 99){
			
			SwarmAchievement.unlock(15966);
		}else if (kills == 999){
			SwarmAchievement.unlock(15968);
		}
		else{
			
			int newKills = kills ++;
			String newKillString = Integer.toString(newKills);
			if (Swarm.isLoggedIn()) {
			    Swarm.user.saveCloudData("kills", ""+newKillString);
			}
		}
		
		
	}
	
	public void moneyAchievment(int moneySpent){
		
		if(moneySpent == 0){
			
			SwarmAchievement.unlock(15964);
		}
	}
	
	public void gameEndAchievement(int waves,boolean submitScore){
		if(waves == 10){
			
			SwarmAchievement.unlock(15960);
		}
		if(submitScore){
			
			SwarmAchievement.unlock(15976);
		}
		classic = false;
		SwarmAchievement.isUnlocked(15954, new AchievementUnlockedCB() {


			@Override
			public void achievementUnlocked(boolean unlocked, Date arg1) {
				
				if(unlocked){
					
					classic = true;
				}
			}});
		SwarmAchievement.isUnlocked(15956 , new AchievementUnlockedCB() {


			@Override
			public void achievementUnlocked(boolean unlocked, Date arg1) {
				
				if(unlocked){
					
					time = true;
				}
			}});
		
		SwarmAchievement.isUnlocked(15956 , new AchievementUnlockedCB() {


			@Override
			public void achievementUnlocked(boolean unlocked, Date arg1) {
				
				if(unlocked){
					
					nightmare = true;
				}
			}});
		
		if(classic && time && nightmare){
			
			SwarmAchievement.unlock(15974);
		}
		
		
	
	
	}


	public void TokenAchievement(){
		SwarmAchievement.unlock(16207);
		
	}
	
	public void upgradeAchievement(int upgrade){
		
		if(android){
		switch(upgrade){
			
			case 0 : SwarmAchievement.unlock(16211);
					 break;
			case 1 : SwarmAchievement.unlock(16213);
					 break;
			case 2: SwarmAchievement.unlock(16209);
					break;
			case 3: SwarmAchievement.unlock(16215 );
					break;
			case 4: SwarmAchievement.unlock(16217);
					break;
			
			
		
		}
		}
	}
	
	public void addEliteKill(){
		
		
		SwarmAchievement.unlock(16219);
	}
	
	public void muteAudio(){
		
		SwarmAchievement.unlock(15970);
	}
}

package com.ReedSmith.DefendThePlanet;

import com.BeefGames.PlutoWasAPlanetOnce.DefendThePlanet;
import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;

public class Main {
	public static void main(String[] args) {
		LwjglApplicationConfiguration cfg = new LwjglApplicationConfiguration();
		cfg.title = "Defend The Planet" + DefendThePlanet.VERSION;
		cfg.useGL20 = true;
		cfg.width = 1080;
		cfg.height = 720;
		//cfg.vSyncEnabled = false;
		
		
		new LwjglApplication(new DefendThePlanet(), cfg);
	}
}

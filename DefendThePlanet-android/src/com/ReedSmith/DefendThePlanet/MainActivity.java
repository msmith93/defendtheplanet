package com.ReedSmith.DefendThePlanet;

import android.os.Bundle;

import com.BeefGames.PlutoWasAPlanetOnce.DefendThePlanet;
import com.badlogic.gdx.backends.android.AndroidApplication;
import com.badlogic.gdx.backends.android.AndroidApplicationConfiguration;
import com.swarmconnect.Swarm;

public class MainActivity extends AndroidApplication {
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
        AndroidApplicationConfiguration cfg = new AndroidApplicationConfiguration();
        cfg.useGL20 = true;
        
        initialize(new DefendThePlanet(), cfg);
        Swarm.setActive(this);
    }
    
 // Add everything below here too
    public void onResume() {
        super.onResume();
        Swarm.setActive(this);
        
        Swarm.setAllowGuests(true);
        Swarm.enableAlternativeMarketCompatability();
        Swarm.init(this, 7392, "87fa5a358305def37f81e07962480104");
    }

    public void onPause() {
        super.onPause();
        Swarm.setInactive(this);
    }
    
 
}